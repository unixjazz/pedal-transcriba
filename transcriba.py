#! /usr/bin/python3
#
# Pedal Transcriba
# VLC-based multimedia player front-end for USB-based pedal.
# Based on the PyQt VLC player created by the VideoLAN team.
#
# Copyright (C) 2009-2010 the VideoLAN team
# Copyright (C) 2017 LF Murillo
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.

import sys
import time
import os
import vlc
from evdev import InputDevice, ecodes
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QThread, pyqtSignal, pyqtSlot

# Pedal IDs
# vendor = "0x2341"
# product = "0x8036"

# TODO: auto-detect supported boards!
DEV_PATH = '/dev/input/by-id/usb-Arduino_LLC_Arduino_Leonardo-event-if02'

try:
   pedaldev = InputDevice(DEV_PATH)
except:
   print("No pedal device found!")
   sys.exit()

# Audio/video player
class Player(QtGui.QMainWindow):
   def __init__(self, master=None):
      QtGui.QMainWindow.__init__(self, master)
      self.setWindowTitle("Pedal Transcriba")
      # creating a basic vlc instance
      self.instance = vlc.Instance()
      # creating an empty vlc media player
      self.MediaPlayer = self.instance.media_player_new()
      self.createUI()
      self.isPaused = False

      # Slots for thread signals
      self.pedalthread = PedalTrans()
      self.pedalthread.pitchDownTrigger.connect(self.pitchDown)
      self.pedalthread.back5Trigger.connect(self.back5)
      self.pedalthread.playPauseTrigger.connect(self.playPause)
      self.pedalthread.timecodeTrigger.connect(self.timecode)
      self.pedalthread.jump5Trigger.connect(self.jump5)
      self.pedalthread.pitchUpTrigger.connect(self.pitchUp)
      self.pedalthread.start()

   def createUI(self):
      self.Widget = QtGui.QWidget(self)
      self.setCentralWidget(self.Widget)
      #self.setStyleSheet("./css/design.css")

      # In this widget, the video will be drawn
      self.VideoFrame = QtGui.QFrame()
      self.Palette = self.VideoFrame.palette()
      self.Palette.setColor (QtGui.QPalette.Window,
                             QtGui.QColor(0,0,0))
      self.VideoFrame.setPalette(self.Palette)
      self.VideoFrame.setAutoFillBackground(True)

      self.PositionSlider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
      self.PositionSlider.setToolTip("Position")
      self.PositionSlider.setMaximum(1000)
      self.connect(self.PositionSlider,
                   QtCore.SIGNAL("sliderMoved(int)"), self.setPosition)

      self.HButtonBox = QtGui.QHBoxLayout()
      self.LoadButton = QtGui.QPushButton("Load File")
      self.HButtonBox.addWidget(self.LoadButton)
      self.connect(self.LoadButton, QtCore.SIGNAL("clicked()"),
                   self.openFile)

      self.PitchDownButton = QtGui.QPushButton("Pitch Down")
      self.HButtonBox.addWidget(self.PitchDownButton)
      self.connect(self.PitchDownButton, QtCore.SIGNAL("clicked()"),
                   self.pitchDown)

      self.BWButton = QtGui.QPushButton("<<")
      self.HButtonBox.addWidget(self.BWButton)
      self.connect(self.BWButton, QtCore.SIGNAL("clicked()"),
                   self.back5)

      self.PlayButton = QtGui.QPushButton("Play")
      self.HButtonBox.addWidget(self.PlayButton)
      self.connect(self.PlayButton, QtCore.SIGNAL("clicked()"),
                   self.playPause)

      self.StopButton = QtGui.QPushButton("Stop")
      self.HButtonBox.addWidget(self.StopButton)
      self.connect(self.StopButton, QtCore.SIGNAL("clicked()"),
                   self.stop)

      self.TimecodeButton = QtGui.QPushButton("Timecode")
      self.HButtonBox.addWidget(self.TimecodeButton)
      self.connect(self.TimecodeButton, QtCore.SIGNAL("clicked()"),
                   self.timecode)

      self.FWButton = QtGui.QPushButton(">>")
      self.HButtonBox.addWidget(self.FWButton)
      self.connect(self.FWButton, QtCore.SIGNAL("clicked()"),
                   self.jump5)

      self.PitchUpButton = QtGui.QPushButton("Pitch Up")
      self.HButtonBox.addWidget(self.PitchUpButton)
      self.connect(self.PitchUpButton, QtCore.SIGNAL("clicked()"),
                   self.pitchUp)

      self.HButtonBox.addStretch(1)
      self.VolumeSlider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
      self.VolumeSlider.setMaximum(100)
      self.VolumeSlider.setValue(self.MediaPlayer.audio_get_volume())
      self.VolumeSlider.setToolTip("Volume")
      self.HButtonBox.addWidget(self.VolumeSlider)
      self.connect(self.VolumeSlider,
                   QtCore.SIGNAL("valueChanged(int)"), self.setVolume)

      self.VBoxLayout = QtGui.QVBoxLayout()
      self.VBoxLayout.addWidget(self.VideoFrame)
      self.VBoxLayout.addWidget(self.PositionSlider)
      self.VBoxLayout.addLayout(self.HButtonBox)

      self.Widget.setLayout(self.VBoxLayout)

      self.Timer = QtCore.QTimer(self)
      self.Timer.setInterval(500)
      self.connect(self.Timer, QtCore.SIGNAL("timeout()"),
                   self.updateUI)

   def playPause(self):
      if self.MediaPlayer.is_playing():
         self.MediaPlayer.pause()
         self.PlayButton.setText("Play")
         self.isPaused = True
      else:
         if self.MediaPlayer.play() == -1:
            self.openFile()
            return
         self.MediaPlayer.play()
         self.PlayButton.setText("Pause")
         self.Timer.start()
         self.isPaused = False

   def stop(self):
      self.MediaPlayer.stop()
      self.PlayButton.setText("Play")

   def back5(self):
      self.MediaPlayer.set_time(self.MediaPlayer.get_time() - 5000)

   def jump5(self):
      self.MediaPlayer.set_time(self.MediaPlayer.get_time() + 5000)

   def pitchDown(self):
      self.MediaPlayer.set_rate(self.MediaPlayer.get_rate() - 0.10)
      print(self.MediaPlayer.get_rate())

   def pitchUp(self):
      self.MediaPlayer.set_rate(self.MediaPlayer.get_rate() + 0.10)
      print(self.MediaPlayer.get_rate())

   def timecode(self):
      timefound = self.MediaPlayer.get_time()
      sec = timefound / 1000
      th = sec / 3600
      tm = sec % 3600 / 60
      ts = sec % 60
      timecode = "%02d:%02d:%02d" % (th, tm, ts)
      timecode = "[" + timecode + "] "
      print("Timecode: %s" % timecode)
      clipboard.setText(timecode)

   def openFile(self):
      filename = QtGui.QFileDialog.getOpenFileName(self,
                                                   "Open File",
                                                   os.path.expanduser('~'))
      if not filename:
          return

      # create the media
      self.media = self.instance.media_new(filename)
      # put the media in the media player
      self.MediaPlayer.set_media(self.media)

      # parse the metadata of the file
      self.media.parse()
      # set the title of the track as window title
      self.setWindowTitle(self.media.get_meta(0))

      # the media player has to be 'connected' to the QFrame
      # (otherwise a video would be displayed in it's own window)
      # this is platform specific!
      # you have to give the id of the QFrame (or similar object) to
      # vlc, different platforms have different functions for this
      if sys.platform.startswith('linux'): # for Linux using the X Server
          self.MediaPlayer.set_xwindow(self.VideoFrame.winId())
      elif sys.platform == "win32": # for Windows
          self.MediaPlayer.set_hwnd(self.VideoFrame.winId())
      elif sys.platform == "darwin": # for MacOS
          self.MediaPlayer.set_agl(self.VideoFrame.windId())
      self.playPause()

   def setVolume(self, Volume):
      self.MediaPlayer.audio_set_volume(Volume)

   def setPosition(self, Position):
      # setting the position to where the slider was dragged
      self.MediaPlayer.set_position(Position / 1000.0)
      # the vlc MediaPlayer needs a float value between 0 and 1, Qt
      # uses integer variables, so you need a factor; the higher the
      # factor, the more precise are the results
      # (1000 should be enough)

   def updateUI(self):
      # setting the slider to the desired position
      self.PositionSlider.setValue(self.MediaPlayer.get_position() * 1000)

      if not self.MediaPlayer.is_playing():
          # no need to call this function if nothing is played
          self.Timer.stop()
          if not self.isPaused:
              # after the video finished, the play button stills shows
              # "Pause", not the desired behavior of a media player
              # this will fix it
              self.stop()

class PedalTrans(QtCore.QThread):
   # signals for GUI events + functions
   pitchDownTrigger = QtCore.pyqtSignal()
   back5Trigger = QtCore.pyqtSignal()
   playPauseTrigger = QtCore.pyqtSignal()
   timecodeTrigger = QtCore.pyqtSignal()
   jump5Trigger = QtCore.pyqtSignal()
   pitchUpTrigger = QtCore.pyqtSignal()

   def __init__(self):
      QtCore.QThread.__init__(self)

   def __del__(self):
      self.wait()

   # TODO: nested ifs are hard to read...
   # write a proper function to handle pedal events
   def run(self):
      while True:
      # codes are 'evdev' button codes
         for event in pedaldev.read_loop():
            if event.code == 288 and event.value == 1:
               press = event.timestamp()
            elif event.code == 288 and event.value == 0:
               release = event.timestamp()
               bounce = release - press
               if bounce > 0.1:
                  self.pitchDownTrigger.emit()
            elif event.code == 289 and event.value == 1:
               press = event.timestamp()
            elif event.code == 289 and event.value == 0:
               release = event.timestamp()
               bounce = release - press
               if bounce > 0.1:
                  self.back5Trigger.emit()
            elif event.code == 290 and event.value == 1:
               press = event.timestamp()
            elif event.code == 290 and event.value == 0:
               release = event.timestamp()
               bounce = release - press
               if bounce > 0.1:
                  self.playPauseTrigger.emit()
            elif event.code == 291 and event.value == 1:
               press = event.timestamp()
            elif event.code == 291 and event.value == 0:
               release = event.timestamp()
               bounce = release - press
               if bounce > 0.1:
                  self.timecodeTrigger.emit()
            elif event.code == 292 and event.value == 1:
               press = event.timestamp()
            elif event.code == 292 and event.value == 0:
               release = event.timestamp()
               bounce = release - press
               if bounce > 0.1:
                  self.jump5Trigger.emit()
            elif event.code == 293 and event.value == 1:
               press = event.timestamp()
            elif event.code == 293 and event.value == 0:
               release = event.timestamp()
               bounce = release - press
               if bounce > 0.1:
                  self.pitchUpTrigger.emit()

if __name__ == "__main__":
   app = QtGui.QApplication(sys.argv)
   clipboard = app.clipboard()
   MediaPlayer = Player()
   MediaPlayer.show()
   MediaPlayer.resize(780, 50)
   sys.exit(app.exec_())
