/* 
 * Pedal Transcriba 0.5 
 * --------------------------------------------------------------
 * This code is public domain
 * --------------------------------------------------------------
 * Modified for Sparkfun PRO Micro from Keates' code for joystick 
 * --------------------------------------------------------------
 */

// Pin definitions
const int BUT1_PIN = 4;
const int BUT2_PIN = 5;
const int BUT3_PIN = 6;
const int BUT4_PIN = 7;
const int BUT5_PIN = 8;
const int BUT6_PIN = 9;

// Previous states
boolean button1State = false;
boolean button2State = false;
boolean button3State = false;
boolean button4State = false;
boolean button5State = false;
boolean button6State = false;

// Hold joystick state
JoyState_t joySt;

void setup()
{
  // Pin definitions
  pinMode(BUT1_PIN, INPUT);
  pinMode(BUT2_PIN, INPUT);
  pinMode(BUT3_PIN, INPUT);
  pinMode(BUT4_PIN, INPUT);
  pinMode(BUT5_PIN, INPUT);
  pinMode(BUT6_PIN, INPUT);

  //pinMode(LED_PIN, OUTPUT);
  
  // Use pull-up resistors
  digitalWrite(BUT1_PIN, HIGH);
  digitalWrite(BUT2_PIN, HIGH);
  digitalWrite(BUT3_PIN, HIGH);
  digitalWrite(BUT4_PIN, HIGH);
  digitalWrite(BUT5_PIN, HIGH);
  digitalWrite(BUT6_PIN, HIGH);
  
  // Set joystick initial state
  joySt.buttons = 0;
}

void loop()
{ 
  // Button 1 pressed
  if (!button1State && digitalRead(BUT1_PIN) == LOW)
  {
    button1State = true;
    joySt.buttons |= 1;
  }

  // Button 1 released
  if (button1State && digitalRead(BUT1_PIN) == HIGH)
  {
    button1State = false;
    joySt.buttons &= 254;
  }

  // Button 2 pressed
  if (!button2State && digitalRead(BUT2_PIN) == LOW)
  {
    button2State = true;
    joySt.buttons |= 2;
  }    

  // Button 2 released
  if (button2State && digitalRead(BUT2_PIN) == HIGH)
  {
    button2State = false;
    joySt.buttons &= 253;
  }

  // Button 3 pressed
  if (!button3State && digitalRead(BUT3_PIN) == LOW)
  {
    button3State = true;
    joySt.buttons |= 4;
  }

  // Button 3 released
  if (button3State && digitalRead(BUT3_PIN) == HIGH)
  {
    button3State = false;
    joySt.buttons &= 251;
  }
  
  // Button 4 pressed
  if (!button4State && digitalRead(BUT4_PIN) == LOW)
  {
    button4State = true;
    joySt.buttons |= 8;
  }

  // Button 4 released
  if (button4State && digitalRead(BUT4_PIN) == HIGH)
  {
    button4State = false;
    joySt.buttons &= 247;
  }
  
  // Button 5 pressed
  if (!button5State && digitalRead(BUT5_PIN) == LOW)
  {
    button5State = true;
    joySt.buttons |= 16;
  }

  // Button 5 released
  if (button5State && digitalRead(BUT5_PIN) == HIGH)
  {
    button5State = false;
    joySt.buttons &= 239;
  }
  
  // Button 6 pressed
  if (!button6State && digitalRead(BUT6_PIN) == LOW)
  {
    button6State = true;
    joySt.buttons |= 32;
  }

  // Button 6 released
  if (button6State && digitalRead(BUT6_PIN) == HIGH)
  {
    button6State = false;
    joySt.buttons &= 223;
  }
  
  // Send joystick state
  Joystick.setState(&joySt);
}
