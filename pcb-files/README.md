# Pedal Transcriba PCB

This directory contains the files for making a PCB using a professional fabrication
facility (such as Seeed Studio or OSHpark). We included the files here for demostration
purposes after presenting it at the 4S conference "making and doing". Please note that
we do not recomment fabricating from these files because they use too much material
(and will make your project expensive).

This is just a simple design to inspire your own creations: you can use just the 
microcontroller part to design something much smaller or to use with spare parts and 
materials that are widely available: you can make the structure for the pedal with 
an L-shaped piece of aluminum (from your neighborhood hardware store) or, even, 
make buttons with leftover pieces of fabric!

![3D rendering of the PCB design](pedal_transcriba_brd_v1.png)

What you will find here:

- KiCad PCB design files
- Extra footprints (for capacitive buttons)
- And gerber files

Happy transcribing!

(just kidding...)
