# Pedal Transcriba

An USB-based pedal for audio/video transcription that you can make!

## Getting Started

This application depends on PyQT4, VLC and Python3. We provide VLC,
and you probably have Python3 pre-installed if you are running an
up-to-date OS. Make sure to install `pyqt4` before you proceed, however. 
On Debian and Debian-derived distros, you can type:

`sudo apt install pyqt4`

Second step is to [write the firmware to the MCU](http://inotool.org/quickstart)
that will control your pedal. You can find the code in the `firmware` 
directory. We recommend installing the 'keyboard' version, because 
it makes the pedal buttons accessible over the keyboard as well 
(for your convenience).

After you are done installing the depedencies and flashing your MCU,
plug your USB pedal and launch the script `transcriba.py`. 

You will be presented with a GUI that displays the buttons / functions
that you can control with your pedal (including 'timecode' capture
which is sent to your clipboard so you can swiftly paste into your 
transcription file in the text editor of your choice):

![](img/pedal-transcriba-GUI.png)

## Pinout
 
The QT application was tested with the Pro Micro Leonardo-compatible 
(ATMEGA32u4) board. Here is the pinout / keymap that we use by 
default:

```
Pin     Key Assignment            Function
------------------------------------------------
D04     KEY_LEFTCTRL + KEY_F7     Pause/Play 
D05     KEY_LEFTCTRL + KEY_F8     Get Timecode
D06     KEY_LEFTCTRL + KEY_F9     Jump Forward >> 
D07     KEY_LEFTCTRL + KEY_F10    Jump Backward <<
D08     KEY_LEFTCTRL + KEY_F11    Speed-Up  
D09     KEY_LEFTCTRL + KEY_F12    Slow-Down
```

Feel free to make modifications directly in the firmware, if you 
prefer. You may want to set key combinations that are not being
used by your window manager.

## Making your pedal

After you are done configuring the MCU, you can start working on
the casing. Here you can exercise your imagination and mount the 
MCU literally on anything that can go on the floor (with proper
insulation) and that can you gently step on.

My first (and last) pedal was based on a L-shaped aluminum 
bar (which can be found in any hardware store), a collection of 
buttons, wires, stickers, and an ATmega32U4 MCU (which can be 
found in many hobbyist boards, most notably on the Arduino 
Leonardo):

[Pedal Transcriba prototype](img/pedal-v1.jpg)

You may take the hard way as well, which is to design and run
your own PCB. [Here is a basic example for you to get started](pcb-files/README.md).

## TODO

- Vim & Emacs integration?
- Migrate from PyQT4 to 5
- Auto-detect for controller boards
- Provide pre-compiled firmware files
- Automate firmware upload with Arduino-mk
- Improve build documentation
- What else?

## Licensing

Each dependency has specific licensing requirements, such as VLC and clipboard
integration software. Check their respective source files for further information. 
Pedal Transcriba is distributed under the GPL version 2. For further information, 
read 'LICENSE'. The PCB files are distributed under CERN OHL v1.2, read 'LICENSE'
in the folder `pcb-files` for more details.

*Enjoy Transcribing!*

(just kidding...)

